package Refactoring;

import Refactoring.Model.Packaging.Box;
import Refactoring.Model.Part.Cube;
import Refactoring.Model.Part.Part;
import Refactoring.Model.Part.Sphere;
import Refactoring.Model.Part.Tetrahedron;
import java.util.Collection;
import java.util.List;

public class Application {

    public static void main(String[] args) {
        // Упустимо код отримання списку деталей, задамо його безпосерезньо перед викликом потрібного коду

        // Деталі
        Collection<Part> partsList = List.of(
                new Cube(450, 0.1),
                new Cube(300, 0.2),
                new Sphere( 150, 0.3),
                new Tetrahedron(250, 0.2),
                new Cube(350, 0.2),
                new Sphere(100, 0.1)
        );

        // Коробки
        Collection<Box> boxList = List.of(
                new Box(700),
                new Box(500),
                new Box(500),
                new Box(500),
                new Box(400),
                new Box(400),
                new Box(400),
                new Box(300),
                new Box(300),
                new Box(200)
        );

        int boxPackagingPricePerMeter = 40; // в копійках

        new PartsProcessor().packageParts(partsList, boxList, boxPackagingPricePerMeter); // Підказка: в цьому виклиці
        // складно розібратися з сігнатурою через погані імена зммінних.
    }
}

