package Refactoring.Model.Part;

sealed public abstract class Part permits Cube, Sphere, Tetrahedron {

    private double density; // густина, в грамах на 1 мм³
    final int protectiveLayerThickness = 30;

    public Part ()
    {}

    public Part(double density) {
        this.density = density;
    }

    public double getDensity() {
        return density;
    }

    public void setDensity(double density) {
        this.density = density;
    }

    public double getProtectiveLayerThickness(double weightInGrams) {
        double weightInKilograms = weightInGrams / 1000;
        return weightInKilograms / protectiveLayerThickness; // Підказка: це є магічною цифрою
    }


}

