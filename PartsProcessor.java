package Refactoring;

import Refactoring.Exception.PackagingException;
import Refactoring.Model.Packaging.Box;
import Refactoring.Model.Part.Cube;
import Refactoring.Model.Part.Part;
import Refactoring.Model.Part.Sphere;
import Refactoring.Model.Part.Tetrahedron;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class PartsProcessor {
    Box box = new Box();

    public void packageParts(Collection<Part> parts, Collection<Box> boxes, int boxPackagingPricePerMillimeter) {
        // Наявні коробки
        List<Box> boxesFromSmallToBigList = new ArrayList<>(boxes);

        // Сортуємо ящики від малих до великих, щоб спростити пошук. Будемо перебирати від малих до великих
        boxesFromSmallToBigList.sort(Comparator.comparing(Box::getSide));

        for (Part part : parts) {
            try {
                Box box = selectSmallestBox(part, boxesFromSmallToBigList);
                int boxPrice = box.calculateBoxPrice(box, boxPackagingPricePerMillimeter);
                reportRecommendedPackaging(part, box, boxPrice);
                boxesFromSmallToBigList.remove(box); // Підказка: як ми можемо краще назвати змінну boxesList,
                // щоб була зрозуміла
                // мета цієї операції?
            } catch (PackagingException e) {
                reportUnableToPackage(part);
            }
        }
    }

    private int getVolumeOfDetail (Part part)
    {
                int volume = 0;
        if (part instanceof Cube) {
            int side = ((Cube) part).getSide();
            volume = side * side * side;
        } else if (part instanceof Sphere) {
            int radius = ((Sphere) part).getRadius();
            double sphereVolume = 4d / 3 * Math.PI * radius * radius * radius; // Об'єм кулі рахуємо як 4/3πr³
            volume = (int) Math.ceil(sphereVolume); // Заокруглюємо отримане значення вгору
        } else if (part instanceof Tetrahedron) {
            int side = ((Tetrahedron) part).getSide();
            double tetrahedronVolume = Math.sqrt(2) / 12 * side * side * side; // Об'єм правильного тетраедру: (√2/12)a³
            volume = (int) Math.ceil(tetrahedronVolume); // Заокруглюємо отримане значення вгору.
        }
        return volume;
    }

    private Box selectSmallestBox(Part part, List<Box> boxesSorted) {
        // Залежно від типу деталі розраховуємо для неї потрібний розмір коробки
        int boxSizeRequired = 0;
        if (part instanceof Cube) {
            boxSizeRequired = ((Cube) part).getSide();
        } else if (part instanceof Sphere) {
            boxSizeRequired = ((Sphere) part).getRadius() * 2;
        } else if (part instanceof Tetrahedron) {
            double cubeSide = ((Tetrahedron) part).getSide() / Math.sqrt(2);
            boxSizeRequired = (int) Math.ceil(cubeSide); // Заокруглюємо отримане значення вгору
        }

        int weight = (int) Math.ceil(getVolumeOfDetail(part) * part.getDensity());

        double protectiveLayerThickness = part.getProtectiveLayerThickness(weight);
        int theFullVolumeOfTheLayer = boxSizeRequired;
        theFullVolumeOfTheLayer += protectiveLayerThickness * 2;


        for (Box box : boxesSorted) {
            if (box.getSide() >= theFullVolumeOfTheLayer) {
                return box;
            }
        }
        // Якщо в циклі ми не знайшли жодного підходящого ящика, повідомимо про це за допомогою кидання виключення
        throw new PackagingException();
    }


    private void reportRecommendedPackaging(Part part, Box box, int price) {
        String message = String.format(
                "Detail %s should be put in a box %s. The cost of packaging: %.2f₴",
                part, box, price / 100d);
        System.out.println(message);
    }

    private void reportUnableToPackage(Part part) {
        String message = String.format(
                "Detail %s could not be placed in existing boxes",
                part);
        System.out.println(message);
    }
}

